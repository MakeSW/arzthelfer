#!/usr/bin/env node

const yargs = require('yargs');
const { readBlocks, readLines } = require('./src/asciidoctor/blockenizer');
const { parseBlock, getGraphPlugin } = require('./src/processor/graph');
const { getIndexerPlugin } = require('./src/processor/indexer');

yargs
    .scriptName('arzthelfer')
    .usage('$0 <cmd> [args]')
    .command(
        'graph <file>',
        'Generate Graph images', (yargs) => {
            yargs.positional('file', {
                type: 'string',
                default: null,
                describe: 'the asciidoctor file to parse. If directory is given, all .adoc, .asciidoc, and .asciidoctor files will be parsed.'
            });
         },
         (argv) => {
            const mindMapGraphPlugin = getGraphPlugin();
            mindMapGraphPlugin.execute(argv.file);
         }
    )
    .command(
        'indexer <file> <indexFile>',
        'Generate index files', (yargs) => {
            yargs.positional('file', {
                type: 'string',
                default: null,
                describe: 'the asciidoctor files including the index files which should be generated. If directory is given, all .adoc, .asciidoc, and .asciidoctor files will be parsed.'
            });
            yargs.positional('indexFile', {
                type: 'string',
                default: '.',
                describe: 'the asciidoctor files containing the keywords for indexing',
            });
        },
        (argv) => {
            const indexerPlugin = getIndexerPlugin();
            indexerPlugin.execute(argv.file, argv.indexFile);
        }
    )
    .help()
    .argv;
