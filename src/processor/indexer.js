const fs = require('fs');
const path = require('path');

const { isFileOrDirectoryExisting, getAsciifileDocs, preparePath } = require("../pintora/fileutils");
const { readBlocks, readLines } = require('../asciidoctor/blockenizer');

/**
 * Checks if one of the lines contains the regex include::(.+)\[(.+)\] and the term arzthelfer=index.
 * @param {Array<string>} block 
 * @returns {boolean} true if the block contains at least one arzthelfer index include element.
 */
function isIndexLinkBlock(block) {
    if (!block || !block.length) {
        return false;
    }

    return block.some(line => {
        if (!line.includes('arzthelfer=index')) {
            return false;
        }

        return line.match(/include::(.+)\[(.+)\]/);
    });

}

function parseIndexLinkBlock(block, context) {
    if (!block || !block.length) {
        return;
    }

    const [ line ] = block;
    const matches = line.match(/include::(.+)\[(.+)\]/);
    const [, filePath, attributes] = matches;

    const attributeMap = attributes.split(',')
        .map(attribute => attribute.trim().split('='))
        .filter(([ key, value ]) => key && value)
        .reduce((acc, [key, value]) => {
            if (key === 'keyword') {
                acc.keyword = acc.keyword || [];
                acc.keyword.push(value);
            } else {
                acc[key] = value;
            }

            return acc;
        }, {});

    const indexEntry = {
        path: filePath,
        fileName: path.basename(filePath),
        descriptor: attributeMap,
    };

    if (context.indexerLinks[filePath]) {
        console.warn(`Duplicte definition of file ${filePath}! Multiple definition in
\t${context.indexerLinks[filePath].path}
\toverwriting with definition of ${indexEntry.path}
        `);
    }

    context.indexerLinks[filePath] = indexEntry;
}

function parseBlocksForIndexLinks(block, context) {
    if (!block || !block.length) {
        return;
    }

    if (isIndexLinkBlock(block)) {
        parseIndexLinkBlock(block, context);
        return;
    }
}

/**
 * Checks if any of the lines of the blocks contains the keywords definition
 * @param {Array<string>} block 
 */
function hasTagBlock(block) {
    if (!block || !block.length) {
        return false;
    }

    return block.some(line => line.startsWith(':keywords:'));
}

function isIndexedFileAlreadyProcessed(context) {
    if (!context.indexedFiles.length) {
        return false;
    }

    const lastProcessedIndexedFile = context.indexedFiles[context.indexedFiles.length - 1];
    return lastProcessedIndexedFile.path === context.file;
}

function parseTagBlock(block, context) {
    const keywords = [];
    
    block.forEach(line => {
        if (line.startsWith(':keywords:')) {
            const keywordsLine = line.replace(':keywords:', '').trim();
            const keywordsOfLine = keywordsLine
                .split(',')
                .map(keyword => keyword.trim());
            keywords.push(...keywordsOfLine);
        }
    });

    context.indexedFiles.push({
        path: context.file,
        fileName: path.basename(context.file),
        keywords
    })
}

function parseNonTaggedBlock(context) {
    context.indexedFiles.push({
        path: context.file,
        fileName: path.basename(context.file),
        keywords: []
    });
}

function generateIndices(context) {
    Object.keys(context.indexerLinks).forEach(outputPath => {
        generateIndexFile(outputPath, context)
    });
}

function generateIndexFile(outputPath, context) {
    const keywords = context.indexerLinks[outputPath].descriptor.keyword || [];
    const inputFiles = [];
    const isIndexTarget = keywords.length
        ? (indexedFile) => keywords.some(keyword => indexedFile.keywords.includes(keyword))
        : () => true;

    const outputTemplateLines = [
    ];

    context.indexedFiles.forEach(indexedFile => {
        if (isIndexTarget(indexedFile)) {
            outputTemplateLines.push(`* link:${indexedFile.path}[${indexedFile.fileName}]`);
        }
    });

    preparePath(outputPath);
    fs.writeFileSync(outputPath, outputTemplateLines.join('\n'));
    console.log(`\tgenerated index file ${outputPath}`);
}

function getIndexerPlugin() {
    function isExectuable(command) {
        return command === 'indexer';
    }

    function execute(inputPath, filesToIndexPath) {
        if (!isFileOrDirectoryExisting(inputPath)) {
            console.error(`Input path ${inputPath} does not exist!`);
            return;
        }

        const filePaths = getAsciifileDocs(inputPath);
        const filesToIndexPaths = getAsciifileDocs(filesToIndexPath);

        const context = {
            file: null,
            indexerLinks: {},
            indexedFiles: [],
        };

        filePaths.forEach(filePath => {
            context.file = filePath;

            readBlocks(readLines(filePath), (block) => {
                parseBlocksForIndexLinks(block, context);
            });
        });

        filesToIndexPaths.forEach(filePath => {
            context.file = filePath;
            
            readBlocks(readLines(filePath), (block) => {
                if (isIndexedFileAlreadyProcessed(context)) {
                    return;
                }

                if (hasTagBlock(block)) {
                    parseTagBlock(block, context);
                } else {
                    parseNonTaggedBlock(context);
                }
            });
        });

        console.log('Parsed context', JSON.stringify(context, null, 2));

        generateIndices(context);
    }

    return {
        isExectuable,
        execute,
    }
}

module.exports = {
    getIndexerPlugin,
    isIndexLinkBlock,
    parseIndexLinkBlock,
};
