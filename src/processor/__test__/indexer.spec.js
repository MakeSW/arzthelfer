const { isIndexLinkBlock } = require("../indexer");

describe('indexer', () => {
    const randoLine = (length) => 'a'.repeat(length);

    describe('isIndexLinkBlock', () => {
        test('correct detection of links', () => {
            const blocks = [
                [
                    randoLine(80),
                    randoLine(40) + ' link:generated/some_file.asciidoc[arzthelfer=index,keywords=34]' + randoLine(30),
                    randoLine(50),
                    randoLine(30),
                ],
                [
                    randoLine(80),
                    randoLine(40) + ' link:generated/some_file.asciidoc[keywords=34,arzthelfer=index]' + randoLine(30),
                ],
                [
                    'link:generated/some_file.asciidoc[arzthelfer=index]',
                ]
            ];

            blocks.forEach(block => {
                expect(isIndexLinkBlock(block)).toBe(true);
            });
        });

        test ('correct detection of no links', () => {
            const blocks = [
                [
                    randoLine(80),
                    randoLine(50),
                    randoLine(30),
                ],
                [
                    randoLine(80),
                ],
                [],
                [
                    'link:generated/some_file.asciidoc[keywords=index]',
                ],
                [
                    'link:generated/some_file.asciidoc',
                ]
            ];

            blocks.forEach(block => {
                expect(isIndexLinkBlock(block)).toBe(false);
            });
        });
    });

});