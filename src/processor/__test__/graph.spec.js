const { isListBlock, parseListBlock, getListItemText, getLevelOfListItem, isImageBlock, parseImageBlock } = require("../graph");

describe('graph', () => {
    // basically any block which is not a list block and does not contain a artzhelfer passthrough block
    const NON_ARZTBLOCK_LIST = `* item 1
    * item 2
    ** item 2.1
    * item 3
    ** item 3.1
    *** item 3.1.1
    * item 4`.split('\n');

    // any list block containing a arzthelfer passthrough block
    const ARZTBLOCK_LIST = `[[pass:arzthelfer,id=arzthelfer-1]]
    * item 1
    * item 2
    ** item 2.1
    * item 3
    ** item 3.1
    *** item 3.1.1
    * item 4`.split('\n');

    const FLAT_ARZTBLOCK_LIST = `[[pass:arzthelfer,id=arzthelfer-1]]
    * item 1
    * item 2
    * item 3
    * item 4`.split('\n');
    
    const DEEP_ARZTBLOCK_LIST = `[[pass:arzthelfer,id=arzthelfer-1]]
    * item 1
    ** item 1.1
    *** item 1.1.1
    **** item 1.1.1.1
    **** item 1.1.1.2
    **** item 1.1.1.3
    ***** item 1.1.1.1.1
    ***** item 1.1.1.1.2
    ***** item 1.1.1.1.1
    *** item 1.1.2
    *** item 1.1.3
    * item 2
    * item 3
    ** item 3.1
    *** item 3.1.1
    *** item 3.1.2
    ** item 3.2
    *** item 3.2.1
    *** item 3.2.2
    *** item 3.2.3
    * item 4`.split('\n');

    const MULTI_LINE_ITEM_ARZTBLOCK_LIST = `[[pass:arzthelfer,id=arzthelfer-1]]
    * item 1
    * item 2
    ** item 2.1
    * item 3
    ** item 3.1
    +
    Some Additional Content
    +
    And another Line!
    *** item 3.1.1
    * item 4`.split('\n');

    describe('isListBlock', () => {
        test ('should return false for non-arzthelfer list blocks', () => {
            expect(isListBlock(NON_ARZTBLOCK_LIST)).toBeFalsy();
        });

        test ('should return true for arzthelfer list blocks', () => {
            expect(isListBlock(ARZTBLOCK_LIST)).toBeTruthy();
        });

        test ('should return true for multiline arzthelfer list blocks', () => {
            expect(isListBlock(MULTI_LINE_ITEM_ARZTBLOCK_LIST)).toBeTruthy();
        });
    });

    describe('getListItemText', () => {
        test('stripping of characters', () => {
            const items = [
                '     * item 1  ',
                '  ** item 1',
                '. item 1',
                '.     item 1   ',
                '  -   item 1  ',
            ];

            items.forEach(item => {
                expect(getListItemText(item)).toEqual('item 1');
            });
        });

        test('proper parsing of anchor links', () => {
            const anchoredText = '*. <<some-id, Some ID Label>>';
            expect(getListItemText(anchoredText)).toEqual('Some ID Label');
        });

        test('proper parsing of http-links with label', () => {
            const httpLink = '** https://some.domain.com/berti/huber?asdf=efef&afef[And The real Label]';
            expect(getListItemText(httpLink)).toEqual('And The real Label');
        });
    });

    describe('getLevelOfListItem', () => {
        test('correct determination of nesting level', () => {
            const fixture = [
                [ '* item 1', 1 ],
                [ '  ***    item 1', 3 ],
                [ '  -- item 1 ', 2 ],
                [ '. item 1', 1 ],
                [ '***** item 1', 5 ],
            ];

            fixture.forEach(([item, level]) => {
                expect(getLevelOfListItem(item)).toEqual(level);
            });
        });

        test('should return 0 for too many levels', () => {
            let line = '';

            for (let i = 0; i < 101; i++) {
                line += '*';
            }

            line += ' item 1';

            expect(getLevelOfListItem(line)).toEqual(0);
        });
    });

    describe('parseListBlock', () => {
        test('should return flat list', () => {
            const context = {};
            parseListBlock(FLAT_ARZTBLOCK_LIST, context);

            expect(context.lists['arzthelfer-1']).toBeTruthy();
            expect(context.lists['arzthelfer-1'].items).toBeTruthy();
            expect(context.lists['arzthelfer-1'].items.length).toEqual(4);
        });

        test('should parse nested list and build tree structure', () => {
            const context = {};
            parseListBlock(ARZTBLOCK_LIST, context);

            expect(context.lists['arzthelfer-1']).toBeTruthy();
            expect(context.lists['arzthelfer-1'].items.length).toEqual(4);
            expect(context.lists['arzthelfer-1'].items[0].children.length).toEqual(0);
            expect(context.lists['arzthelfer-1'].items[1].children.length).toEqual(1);
            expect(context.lists['arzthelfer-1'].items[2].children.length).toEqual(1);
            expect(context.lists['arzthelfer-1'].items[2].children[0].children.length).toEqual(1);
            expect(context.lists['arzthelfer-1'].items[3].children.length).toEqual(0);
        });

        test('correct parsing of deep nested list', () => {
            const context = {};
            parseListBlock(DEEP_ARZTBLOCK_LIST, context);

            expect(context).toEqual({
                "lists": {
                  "arzthelfer-1": {
                    "id": "arzthelfer-1",
                    "items": [
                      {
                        "label": "item 1",
                        "children": [
                          {
                            "label": "item 1.1",
                            "children": [
                              {
                                "label": "tem 1.1.1",
                                "children": [
                                  {
                                    "label": "em 1.1.1.1",
                                    "children": []
                                  },
                                  {
                                    "label": "em 1.1.1.2",
                                    "children": []
                                  },
                                  {
                                    "label": "em 1.1.1.3",
                                    "children": [
                                      {
                                        "label": "m 1.1.1.1.1",
                                        "children": []
                                      },
                                      {
                                        "label": "m 1.1.1.1.2",
                                        "children": []
                                      },
                                      {
                                        "label": "m 1.1.1.1.1",
                                        "children": []
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "label": "tem 1.1.2",
                                "children": []
                              },
                              {
                                "label": "tem 1.1.3",
                                "children": []
                              }
                            ]
                          }
                        ]
                      },
                      {
                        "label": "item 2",
                        "children": []
                      },
                      {
                        "label": "item 3",
                        "children": [
                          {
                            "label": "item 3.1",
                            "children": [
                              {
                                "label": "tem 3.1.1",
                                "children": []
                              },
                              {
                                "label": "tem 3.1.2",
                                "children": []
                              }
                            ]
                          },
                          {
                            "label": "item 3.2",
                            "children": [
                              {
                                "label": "tem 3.2.1",
                                "children": []
                              },
                              {
                                "label": "tem 3.2.2",
                                "children": []
                              },
                              {
                                "label": "tem 3.2.3",
                                "children": []
                              }
                            ]
                          }
                        ]
                      },
                      {
                        "label": "item 4",
                        "children": []
                      }
                    ]
                  }
                }
              });
        });
    });

    describe('isImageBlock', () => {
        test('correct detection of arzthelfer image blocks', () => {
            const imageBlocks = [
                'image::some/relative/path.png[arzthelfer=mindmap,arzthelfer_source=some_id,additional_info=berti]',
                'image::/some/absolute/path.png[arzthelfer=mindmap,arzthelfer_source=some_id]',
                'image::/some/absolute/path.png[different=order,arzthelfer=mindmap,berti=huber,arzthelfer_source=some_id]',
            ];

            imageBlocks.forEach(imageBlock => {
                expect(isImageBlock([imageBlock])).toBeTruthy();
            });
        });

        test('correct false-detection of non-arzthelfer image blocks', () => {
            const imageBlocks = [
                'image::some/relative/path.png[arzthelfer_source=some_id,additional_info=berti]',
                'image::/some/absolute/path.png[arzthelfer=class,arzthelfer_source=some_id]',
                'image::/some/absolute/path.png[]',
            ];

            imageBlocks.forEach(imageBlock => {
                expect(isImageBlock([imageBlock])).toBeFalsy();
            });
        });
    });

    describe('parseImageBlock', () => {
        test('parsing of artzelfer image block', () => {
            const imageBlock = 'image::/some/absolute/path.png[description text,title=berti,additional_info=123,arthelfer=mindmap,arzthelfer_source=some_id,berti=123]';
            const context = {};

            parseImageBlock([ imageBlock ], context);

            expect(context.images).toBeTruthy();
            expect(context.images['some_id']).toBeTruthy();

            const image = context.images['some_id'][0];

            expect(image.arzthelfer_source).toEqual('some_id');
            expect(image.title).toEqual('berti');
            expect(image.path).toEqual('/some/absolute/path.png');
            expect(image.additional_info).toEqual('123');
            expect(image.fileName).toEqual('path.png');
        });
    });
});
