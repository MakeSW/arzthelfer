const path = require('path');
const { getAsciifileDocs, isFileOrDirectoryExisting, getAsciifileDocsLineIterator } = require('../pintora/fileutils');
const { readBlocks, readLines } = require('../asciidoctor/blockenizer');

const Pintora = require('../pintora/mindmapper');

const LIST_ITEM_PREFIX = [ '*', '-', '.' ];
const MULTILINE_SEPARATORS = [ '+' ];

function parseBlock(block, context) {
    if (!block || !block.length) {
        return;
    }

    if (isImageBlock(block)) {
        parseImageBlock(block, context);
        return;
    } 

    if (isListBlock(block)) {
        parseListBlock(block, context);
        return;
    }
}

function isImageBlock(block) {
    if (!block || block.length !== 1) {
        return false;
    }

    const [ line ] = block;

    return line.includes('image::')
        && line.includes('arzthelfer=mindmap') // only supports mindmap for now
        && line.includes('arzthelfer_source=');
}

function isListBlockItem(line) {
    const start = line.trim().charAt(0);
    const isListItem = LIST_ITEM_PREFIX.includes(start);

    return isListItem
}

function isMultiLineSeparator(line) {
    const start = line.trim().charAt(0);
    const isSeparator = MULTILINE_SEPARATORS.includes(start);

    return isSeparator;
}

/**
 * Checks if the block is a list block and contains a arzthelfer passthrough block at the beginning (first line).
 * 
 * @param {Array<string>} block 
 * @returns boolean true if the block is a arztblock list block, false otherwise
 */
function isListBlock(block) {
    if (!block || !block.length || block.length < 2) {
        return false;
    }

    const [ passthrough, ...listLines ]  = block;

    if (!isPassthroughLine(passthrough)) {
        return false;
    }

    for (let i = 0; i < listLines.length; i++) {
        const isListItem = isListBlockItem(listLines[i]);

        if (isListItem) {
            continue;
        } else if (isMultiLineSeparator(listLines[i])) {
            i++;
            continue;
        } else {
            return false;
        }
    }

    return true;
}

function getLevelOfListItem(line) {
    const TO_MANY_LEVELS = 100;
    const start = line.trim();

    let i = 0;

    do {
        if (i > TO_MANY_LEVELS) {
            // signal error
            return 0;
        }

        if (LIST_ITEM_PREFIX.includes(start.charAt(i))) {
            i++;
            continue;
        }

        return i;
    } while(true);

    return 0;
}

function getListItemText(line) {
    let strippedLineText = (line || '').trim();

    for (let i = 0; i < line.length; i++) {
        const char = line.charAt(i);

        if (LIST_ITEM_PREFIX.includes(char)) {
            strippedLineText = strippedLineText.substring(1);
        }
    }

    strippedLineText = strippedLineText.trim();

    const isAnchorLink = strippedLineText.includes('<<') && strippedLineText.includes('>>');

    if (isAnchorLink) {
        const [ , anchor ] = strippedLineText.match(/<<(.*)>>/);
        const anchorParts = anchor.split(',');

        if (anchorParts.length === 2) {
            return anchorParts[1].trim();
        }

        return anchor.trim();
    }

    const hyperLinkRegex = (/[^\s]{1,6}:\/\/[^\[]+\[([^\[]+)\]/g);
    const matchedHyperLink = hyperLinkRegex.exec(strippedLineText);
    const isAsciidocHyperlink = matchedHyperLink && matchedHyperLink.length === 2;

    if (isAsciidocHyperlink) {
        return (matchedHyperLink[1] || '').trim();
    }

    return strippedLineText;
}

function parseImageBlock(block, context) {
    const imageRegex = (/image::(.+)\[(.+)\]/g);
    
    const [ line ] = block;
    const matchedImage = imageRegex.exec(line);

    if (!matchedImage || matchedImage.length !== 3) {
        return;
    }

    const [ , filePath, descriptorToken ] = matchedImage;
    const fileName = path.basename(filePath);
    const descriptorParts = (descriptorToken || '').split(',');
    const descriptor = {};

    descriptorParts
        .map(part => part.split('='))
        .forEach(([ key, value ]) => {
            if (!value) {
                descriptor.description = key;
            } else {
                descriptor[key] = value;
            }
        });

    if (!descriptor.arzthelfer_source) {
        return;
    }

    const id = descriptor.arzthelfer_source;

    const image = {
        ...descriptor,
        path: filePath,
        fileName,
        id,
    };

    context.images = context.images || {};
    context.images[id] = context.images[descriptor.id] || [];
    context.images[id].push(image);
}

function isPassthroughLine(line) {
    return line.startsWith('[[pass:arzthelfer') && line.endsWith(']]');
}

function parsePassthroughDescriptor(line) {
    const arzthelferPassthroughBlockRegex = /\[\[pass:arzthelfer,(.+)\]\]/g;
    const matchedResult =  arzthelferPassthroughBlockRegex.exec(line);

    if (!matchedResult) {
        return null;
    }

    const variableDefinitions = matchedResult[1];

    if (!variableDefinitions) {
        return null;
    }

    const descriptor = {};
    const variableDefinitionParts = variableDefinitions.split(',');

    variableDefinitionParts
        .map(part => part.split('='))
        .forEach(([ key, value ]) => descriptor[key] = value);


    return descriptor;
}

function parseListBlock(block, context) {
    const itemsStack = [];
    const list = [];

    const [ passthrough, ...listLines ]  = block;

    const descriptor = parsePassthroughDescriptor(passthrough);

    for (const line of listLines) {
        if (isListBlockItem(line)) {
            const label = getListItemText(line);
            const level = getLevelOfListItem(line);
            const listItem = {
                label,
                children: [],
            };

            const expectedStackHeight = level - 1;

            while (itemsStack.length > expectedStackHeight && itemsStack.length > 0) {
                itemsStack.pop();
            }

            if (expectedStackHeight === 0) {
                list.push(listItem);
            } else if (itemsStack.length > 0) {
                itemsStack[itemsStack.length - 1].children.push(listItem);
            } else {
                throw new Error('Invalid list structure!');
            }

            itemsStack.push(listItem);
        }
    }

    context.lists = context.lists || {};
    context.lists[descriptor.id] = {
        ...descriptor,
        items: list,
    };
}

function getGraphPlugin() {
    const context = {
        file: null,
        // a list of images containing all three required attributes
        images: {},
        // a map of lists identified by a pass-through id
        lists: {},
    };

    function isExectuable(command) {
        return command === 'graph';
    }

    async function execute(inputPath) {
        await Pintora.initialize();

        if (!isFileOrDirectoryExisting(inputPath)) {
            console.error(`${inputPath} does not exist!`);
            return;
        }

        const filePaths = getAsciifileDocs(inputPath);

        filePaths.forEach(filePath => {
            context.file = filePath;
            parseBlock(readLines(filePath), context);
            console.log(`parsed file ${filePath}`);
        });
        
        readBlocks(getAsciifileDocsLineIterator(inputPath), (block) => {
            parseBlock(block, context);
        });

        Object.values(context.images).forEach(async (images) => {
            const image = images[0];
            const imageId = image.id;
            const path = image.path;
            const parsedList = context.lists[imageId];

            if (parsedList) {
                const mindMapCode = Pintora.generateMindMapCode(parsedList);
                await Pintora.generatePng(image, mindMapCode);
                console.log(`Mindmap generated for ${imageId}!\n\t${path}`);
                return;
            }

            console.warn(`Found arzthelfer image with id ${imageId} but no list with the same id!`);
        });
    }

    return {
        isExectuable,
        execute,
    };
}

module.exports = {
    getGraphPlugin,
    parseListBlock,
    parseBlock,
    isListBlock,
    getListItemText,
    getLevelOfListItem,
    isImageBlock,
    parseImageBlock,
};
