const linebyline = require('linebyline');
const fs = require('fs');

function* readLines (path) {
    const allLines = fs.readFileSync(path).toString().split('\n');

    for (const line of allLines) {
        yield line;
    }
}

function readBlocks(getLinesGenerator, onBlockFound) {
    let linesOfBlock = [];

    for (const line of getLinesGenerator) {
        const sanitizedLine = (line || '').trim();

        if (sanitizedLine === '') {
            if (linesOfBlock.length === 0) {
                continue;
            }

            onBlockFound(linesOfBlock);
            linesOfBlock = [];
        } else {
            linesOfBlock.push(sanitizedLine);
        }
    }

    if (linesOfBlock.length) {
        onBlockFound(linesOfBlock);
    
    }
}

module.exports = {
    readBlocks,
    readLines,
};