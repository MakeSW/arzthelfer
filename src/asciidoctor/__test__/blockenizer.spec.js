const { readBlocks } = require("../blockenizer");

describe('blockenizer', () => {
    describe('readBlocks', () => {
        const ASCIIDOC_INPUT = `
        # Header 1

        Some text

        ## Header 2

        Some more text

        [[pass:arzthelfer,id=arzthelfer-1]]
        . List Item 1
        . List Item 2
        . List Item 3
        `;

        function* getLinesGenerator() {
            const splittedLines = ASCIIDOC_INPUT.split('\n');

            for (const line of splittedLines) {
                yield line;
            }
        }

        test('readBlocks should read all blocks', () => {
            const blocks = [];

            readBlocks(getLinesGenerator(), (block) => blocks.push(block));

            expect(blocks.length).toBe(5);
        });

        test('readBlocks should trim lines', () => {
            const blocks = [];

            readBlocks(getLinesGenerator(), (block) => blocks.push(block));

            blocks.forEach((block) => {
                block.forEach(line => {
                    const trimmedLine = line.trim();

                    expect(trimmedLine).toEqual(line);
                });
            });
        });
    });
});
