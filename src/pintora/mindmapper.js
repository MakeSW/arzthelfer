const fs = require('fs');
const { preparePath, getImageMimetypeOfFileExtension, getFileExtension } = require('./fileutils');

// workaround for stupid mjs/commonjs compatibility issue
let render;
let PintoraConfig;

function initialize() {
    return new Promise((resolve, reject) => {
        import('@pintora/cli')
            .then(({ default: pintora }) => {
                render = pintora.render;
                PintoraConfig = pintora.PintoraConfig;

                resolve();
            })
            .catch((err) => {
                reject(err);
            });
    });
}

function removeSyntaxElements(line) {
    return (line || '')
        .replace(/[(]/g, '[')
        .replace(/[)]/g, ']');
}

/**
 * Generates valid PintoraJS [mindmap code](https://pintorajs.vercel.app/docs/diagrams/mindmap/) from a parsed asciidoc list.
 * 
 * @param {Object} parsedAsciidocList 
 * @returns {string} valid PintoraJS mindmap code
 */
function generateMindMapCode(parsedAsciidocList) {
    const itemDefinition = [];
    const rootLabel = !!parsedAsciidocList.title
        ? `* ${parsedAsciidocList.title}`
        : '* Mindmap';
    let level = 2;

    parsedAsciidocList.items.forEach((item) => {
        const label = removeSyntaxElements(item.label || '');

        const padding = '*'.repeat(level) + ' ';
        itemDefinition.push(`${padding}${label}`);

        const childItems = getChildItems(item, level + 1);
        itemDefinition.push(...childItems);
    });

    return `mindmap
@param layoutDirection TB
${rootLabel}
${itemDefinition.join('\n')}
`;
}

function getChildItems(item, nestingLevel) {
    if (!item.children) {
        return [];
    }

    const padding = '*'.repeat(nestingLevel) + ' ';

    const lines = [];

    item.children.forEach((child) => {
        const label = removeSyntaxElements(child.label);
        lines.push(`${padding}${label}`);
        const childLines = getChildItems(child, nestingLevel);

        childLines.forEach(line => lines.push(line));
    });

    return lines;
}

async function generatePng({ path }, pintoraSourceCode) {
    preparePath(path);

    const fileExtension = getFileExtension(path);
    const mimeType = getImageMimetypeOfFileExtension(fileExtension);

    const buf = await render({
        mimeType,
        code: pintoraSourceCode,
        width: 800,
        backgroundColor: '#fdfdfd', // use some other background color
    });

    fs.writeFileSync(path, buf);
}

module.exports = {
    generateMindMapCode,
    getChildItems,
    initialize,
    generatePng,
};
