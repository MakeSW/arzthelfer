const { generateMindMapCode } = require("../mindmapper");

describe('mindmapper', () => {
    describe('generateMindMapCode', () => {
        const SIMPLE_LIST_BLOCK_WITH_TITLE = {
            title: 'Bertis Root',
            items: [
                {
                    label: 'Item 1',
                    children: [
                        {
                            label: 'Item 1.1',
                            children: [
                                {
                                    label: 'Item 1.1.1',
                                },
                            ],
                        },
                        {
                            label: 'Item 1.2',
                        },
                    ],
                },
                {
                    label: 'Item 2',
                },
            ],
        };

        test('generateMindMapCode should generate valid mindmap code', () => {
            const generatedMindMapCode = generateMindMapCode(SIMPLE_LIST_BLOCK_WITH_TITLE);
    
            expect(generatedMindMapCode).toBeTruthy();
            expect(generatedMindMapCode).toEqual(`mindmap
@param layoutDirection TB
* Bertis Root
** Item 1
*** Item 1.1
*** Item 1.1.1
*** Item 1.2
** Item 2
`);
        });    
    });
});
