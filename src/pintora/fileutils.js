const fs = require('fs')
const path = require('path')

function getDirectoryOfFile(filePath) {
  return filePath.split('/').slice(0, -1).join('/')
}

function isDirectory(path) {
    return fs.lstatSync(path).isDirectory();
}

function isFileOrDirectoryExisting(path) {
    return fs.existsSync(path);
}

function isAsciidocFile(filePath) {
    const extension = (getFileExtension(filePath) || '').toLowerCase();    
    return [ 'asc', 'asciidoc', 'asciidoctor' ].includes(extension);
}

function getAllAsciidocFilesInDirectory(directoryPath) {
    return fs.readdirSync(directoryPath)
        .filter((fileName) => isAsciidocFile(fileName))
        .map(fileName => path.join(directoryPath, fileName));
}

function isDirectoryExisting(dirPath) {
  return fs.existsSync(dirPath) && fs.lstatSync(dirPath).isDirectory()
}

function createDirectory(dirPath) {
  if (!isDirectoryExisting(dirPath)) {
    fs.mkdirSync(dirPath, { recursive: true })
  }
}

function getFileExtension(filePath) {
    return filePath.split('.').pop();
}

/**
 * Returns the requested mime type of the given file extension. Supported mimetimes are:
 * - image/png
 * - image/jpeg
 * 
 * @param {string} fileExtension 
 * @returns {string} mimetype of the image (eg. 'image/png')
 */
function getImageMimetypeOfFileExtension(fileExtension) {
    if (!fileExtension) {
        throw new Error('fileExtension must not be empty');
    }
    
    const supportedFileExtensionMap = {
        'png': 'image/png',
        'jpg': 'image/jpeg',
        'jpeg': 'image/jpeg',
    };

    const lowerCaseExtension = fileExtension.toLowerCase();

    if (!supportedFileExtensionMap[lowerCaseExtension]) {
        throw new Error(`Unsupported file extension ${fileExtension}`);
    }

    return supportedFileExtensionMap[lowerCaseExtension];
}

function preparePath(path) {
    const directory = getDirectoryOfFile(path);
    createDirectory(directory);
}

function getAsciifileDocs(inputPath) {
    if (isDirectory(inputPath)) {
        return getAllAsciidocFilesInDirectory(inputPath);
    }

    if (fs.existsSync(inputPath)) {
        return [ inputPath ];
    }

    return [];
}

function* getAsciifileDocsLineIterator(inputPath) {
    const docs = getAsciifileDocs(inputPath);

    for (const docPath of docs) {
        const lines = fs.readFileSync(docPath, 'utf8').split('\n');

        for (line of lines) {
            yield line;
        }
    }
}

module.exports = {
    getDirectoryOfFile,
    isDirectoryExisting,
    createDirectory,
    getFileExtension,
    getImageMimetypeOfFileExtension,
    preparePath,
    isDirectory,
    isFileOrDirectoryExisting,
    getAsciifileDocs,
    getAsciifileDocsLineIterator,
};
